# Stickpage JSON-based DB Package for Node.js

Simple Node.js library to querying the StickPage DB without online access to the API server, inspired by [the `naughty-words` package](https://github.com/LDNOOBW/naughty-words-js).

## Installation and Usage

Currently on alpha, but it will be soon be go to beta releases. To install with npm, use:

```bash
npm i --save @stickpage-api/json-db # if you're in yarn, replace npm with yarn.
```

To browse directly, require the json file.

```js
var GGMetadata = require("@stickpage-api/json-db/slushers/gildedguy.json"); // Pulling GG data gone brrrrrr.
console.log(GGMetadata);
```

Need to browse the whole database without requiring the path to that JSON file? Require the whole package.

```js
var stickpageOfflineApi = require("@stickpage-api/json-db");
console.log(stickpageOfflineApi); // WARNING: Too large DB! This might take ages to log to console.
console.log(stickpageOfflineApi.StickPageDB.jade);

// Need 2 lines?
const StickPageDB = require("@stickpage-api/json-db").StickPageDB; 
console.log(StickPageDB.fry);
```

### Alternative Download

If you prefer to download the central JSON database instead, use:

```bash
# You may need to reinstall this package once
# on monthly basis to update your local copy.
# For Heroku users, use the git-clone way instead
# during Docker image builds.
npm i --save github:StickPage-API/Central-DB
```

If you want to edit the `package.json` instead, append this on your `dependencies` section of your app's `package.json`

```json
{
    "dependencies": {
        "@stickpage-api/db-repo": "gitlab:StickPage/Database-Repository"
    }
}
```

For usage, replace `@stickpage-api/json-db` with `@stickpage-api/db-repo` when requiring an path to that JSON file. That package doesn't have an `index.js`, so use the requir-path stuff for now.

## Adding your stick character

We only pull metadata from the official [Stickpage Wiki](https://stickpage.fandom.com). If you want your stick character to be added,
[start your RHG here](http://forums.stickpage.com/showthread.php?33615-Starting-your-RHG) and/or
[start your Duelist here](http://www.hyunsdojo.com/community/viewtopic.php?f=47&t=6178).

Once your stick character is added, [consider checking the documentation] on your next steps.

Sometimes, we may add special categories in some cases (e.g. `slushers` for [Slush Fighters](https://slushinvaders.fandom.com/wiki/Slush_Fighters)). See the documentation for details.

## Basic Documentation for Maintainers

### Release Cycle

Atleast 5 commits from the database repository are needed to be merged to `master` branch, get tagged in Git and get published in the registry. To pull changes from the central database repository (for entry updates and additions), run `npm run db:build`.

In case of entry removals, please [remove the entry manually] or run `npm run db:rebuild` (rebuild may take longer than running the `db:build` script). While the build script can remove the references from the `<category>/index.js`, pajeets can still require the path to that JSON file.

### Versioning

* When changes to files in the `templates` directory and the format was updated across all JSON files or breaking changes to the implemented, bump to the next major number.
* If the database is updated to atleast 10 JSON files, bump to next mirror number.
* For bugfixes or fixes in the JSON files (grammar fixes, name changes), bump to next patch number.

## License

MIT

## Community

Our friendly community can help you if you ran into problems.

* **Telegram**: [Support Chat](https://t.me/stickPageAPISupport)
* **The Pins Team Community Forum**: [StickPage API category](https://community.madebythepins.tk)

[consider checking the documentation]: https://thepinsteam.gitbook.io/stickpage-api-docs/contribute/adding-your-stick-character
[remove the entry manually]: https://thepinsteam.gitbook.io/stickpage-api-docs/json-db-nodejs/building-the-package#removing-entries
