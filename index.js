// Improt everything
module.exports = {};

// For RHGs and Duelists, pull meta from https://stickpage.fandom.com.
// Make sure they're official RHG/Duelist before including them.
module.export['StickPageDB'] = require('./stickpage');

// For stick characters that part of the Slush Invaders series, pull metadata from
// https://TBA
module.export['SlushInvadersDB'] = require('./slush-invaders');

// Lastly, I'mma pull stick characters' creators too!
// I need help collecting data.
module.export['CreatorsDB'] = require('./animators');
